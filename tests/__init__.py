from django.conf import settings

settings.IS_TESTING = True
settings.CELERY_TASK_ALWAYS_EAGER = True