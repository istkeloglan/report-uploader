import os
from ftplib import error_perm

from django.test import TestCase
from mock import patch, Mock

from remote_connection.connection_manager import ExceptionConnectionError
from remote_connection.connection_manager import ExceptionConnectionNotFound
from remote_connection.connection_manager import ExceptionFileNotFound
from remote_connection.connection_manager import ExceptionPermissionError
from remote_connection.connection_manager import SFTPManager
from remote_connection.factories import KnownRemoteConnectionFactory
from remote_connection.factories import RemoteConnectionFactory
from task.factories import PeriodicTaskFactory
from task.factories import ScheduleFactory
from task.factories import TaskFactory


class SFTPManagerTests(TestCase):
    PATH = 'Untitled'

    @classmethod
    def setUp(cls):
        cls.conn = KnownRemoteConnectionFactory()
        cls.schedule = ScheduleFactory()
        cls.pschedule = PeriodicTaskFactory(
            crontab=cls.schedule
        )
        cls.known_task = TaskFactory(
            connection=cls.conn,
            periodic_task=cls.pschedule,
        )

        cls.random_task = TaskFactory(
            connection=RemoteConnectionFactory(),
            periodic_task=cls.pschedule
        )

        with open(cls.PATH, "wt") as f:
            f.write('123')

    @patch('remote_connection.connection_manager.sftp_manager.paramiko.SSHClient')
    def test_login_success(self, MockFTP):
        MockFTP.return_value = Mock()
        MockFTP.return_value.connect.return_value = Mock()

        """ Check the connection is success with correct credentials """
        try:
            SFTPManager(self.known_task, self.PATH).run()
        except ExceptionConnectionError:  # pragma: no cover
            self.fail("FTP Manager failed: Connection raised Exception")

    @patch('remote_connection.connection_manager.sftp_manager.paramiko.SSHClient')
    def test_login_fail(self, MockFTP):
        """
        RemoteConnection object created with random credentials.
        FTP Manager needs to raise the ExceptionConnectionException
        """
        MockFTP.return_value = Mock()
        MockFTP.return_value.connect.side_effect = ExceptionConnectionError

        manager = SFTPManager(self.random_task, self.PATH)
        self.assertRaises(ExceptionConnectionError, manager.run)

    @patch('remote_connection.connection_manager.sftp_manager.paramiko.SSHClient')
    def test_close_connection(self, MockFTP):
        """ Close existing ftp connection """
        MockFTP.return_value = Mock()
        try:
            manager = SFTPManager(self.known_task, self.PATH)
            manager.connect()
            manager.close_connection()
        except ExceptionConnectionNotFound:  # pragma: no cover
            self.fail("FTP Manager failed: close_connection raised Exception")

    def test_run_close_connection_without_connection(self):
        """
        Run close_connection method without connection object.
        In this case, the method needs to raise ExceptionConnectionNotFound exception
        """
        manager = SFTPManager(self.known_task, self.PATH)
        self.assertRaises(ExceptionConnectionNotFound, manager.close_connection)
        os.remove(self.PATH)

    @patch('remote_connection.connection_manager.sftp_manager.paramiko.SSHClient')
    def test_change_directory_in_active_connection(self, MockFTP):
        """
            Once the object has an active connection,
            it should able to change the directory if the path exists in the file system.
            Right after that, it needs to create the file.
        """
        MockFTP.return_value = Mock()
        MockFTP.return_value.stat.return_value = 'None'
        MockFTP.return_value.put.return_value = 'None'

        manager = SFTPManager(self.known_task, self.PATH)
        manager.connect()
        manager.upload_file()
        manager.close_connection()

    @patch('remote_connection.connection_manager.sftp_manager.paramiko.SSHClient')
    def test_change_directory_fail(self, MockFTP):
        """
            In case, the manager raises a path exception
        """
        MockFTP.return_value = Mock()
        MockFTP.return_value.open_sftp.return_value.chdir = Mock(
            side_effect=FileNotFoundError()
        )

        self.known_task.connection.path = '/xyz/twe/yub'
        self.known_task.connection.save()
        manager = SFTPManager(self.known_task, self.PATH)
        manager.run()
        self.assertFalse(manager.status)

    @patch('remote_connection.connection_manager.sftp_manager.paramiko.SSHClient')
    def test_change_permission_error(self, MockFTP):
        """
            Validates put method
        """
        MockFTP.return_value = Mock()

        MockFTP.return_value.open_sftp.return_value.put = Mock(
            side_effect=Exception()
        )

        manager = SFTPManager(self.known_task, self.PATH)
        manager.connect()
        self.assertRaises(ExceptionPermissionError, manager.upload_file)

