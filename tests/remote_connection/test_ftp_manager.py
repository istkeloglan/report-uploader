import os
from ftplib import error_perm

from django.test import TestCase
from mock import patch, Mock

from remote_connection.connection_manager import ExceptionConnectionError
from remote_connection.connection_manager import ExceptionConnectionNotFound
from remote_connection.connection_manager import ExceptionFileNotFound
from remote_connection.connection_manager import ExceptionPermissionError
from remote_connection.connection_manager import FTPManager
from remote_connection.factories import KnownRemoteConnectionFactory
from remote_connection.factories import RemoteConnectionFactory
from task.factories import PeriodicTaskFactory
from task.factories import ScheduleFactory
from task.factories import TaskFactory


class FTPManagerTests(TestCase):
    PATH = 'Untitled'

    @classmethod
    def setUp(cls):
        cls.conn = KnownRemoteConnectionFactory()
        cls.schedule = ScheduleFactory()
        cls.pschedule = PeriodicTaskFactory(
            crontab=cls.schedule
        )
        cls.known_task = TaskFactory(
            connection=cls.conn,
            periodic_task=cls.pschedule,
        )

        cls.random_task = TaskFactory(
            connection=RemoteConnectionFactory(),
            periodic_task=cls.pschedule
        )

        with open(cls.PATH, "wt") as f:
            f.write('123')

    @patch('remote_connection.connection_manager.ftp_manager.FTP')
    def test_login_success(self, MockFTP):
        MockFTP.return_value = Mock()

        """ Check the connection is success with correct credentials """
        try:
            FTPManager(self.known_task, self.PATH).run()
        except ExceptionConnectionError:  # pragma: no cover
            self.fail("FTP Manager failed: Connection raised Exception")

    def test_login_fail(self):
        """
        RemoteConnection object created with random credentials.
        FTP Manager needs to raise the ExceptionConnectionException
        """
        manager = FTPManager(self.random_task, self.PATH)
        self.assertRaises(ExceptionConnectionError, manager.run)

    @patch('remote_connection.connection_manager.ftp_manager.FTP')
    def test_close_connection(self, MockFTP):
        """ Close existing ftp connection """
        MockFTP.return_value = Mock()
        try:
            manager = FTPManager(self.known_task, self.PATH)
            manager.connect()
            manager.close_connection()
        except ExceptionConnectionNotFound:  # pragma: no cover
            self.fail("FTP Manager failed: close_connection raised Exception")

    def test_run_close_connection_without_connection(self):
        """
        Run close_connection method without connection object.
        In this case, the method needs to raise ExceptionConnectionNotFound exception
        """
        manager = FTPManager(self.known_task, self.PATH)
        self.assertRaises(ExceptionConnectionNotFound, manager.close_connection)

    @patch('remote_connection.connection_manager.ftp_manager.FTP')
    def test_change_directory_in_active_connection(self, MockFTP):
        """
            Once the object has an active connection,
            it should able to change the directory if the path exists in the file system.
            Right after that, it needs to create the file.
        """
        MockFTP.return_value = Mock()
        MockFTP.return_value.voidcmd.return_value = 'None'

        manager = FTPManager(self.known_task, self.PATH)
        manager.connect()
        manager.upload_file()
        manager.close_connection()

    @patch('remote_connection.connection_manager.ftp_manager.FTP')
    def test_uploaded_file_not_found(self, MockFTP):
        """ The logic has to delete the file which created for each connection """
        MockFTP.return_value = Mock()
        MockFTP.return_value.voidcmd.return_value = '--- No such file or --'
        manager = FTPManager(self.known_task, self.PATH)
        manager.connect()
        manager.upload_file()
        os.remove(manager.file_path)
        self.assertRaises(ExceptionFileNotFound, manager.close_connection)

    @patch('remote_connection.connection_manager.ftp_manager.FTP')
    def test_change_directory_fail(self, MockFTP):
        """
            In case, connection raises a path exception
        """
        MockFTP.return_value = Mock()
        MockFTP.return_value.cwd.side_effect = [error_perm, None, error_perm, error_perm, None, None]
        MockFTP.return_value.pwd.return_value = '/23'
        MockFTP.return_value.voidcmd.return_value = 'None'
        MockFTP.return_value.size.return_value = 3

        self.known_task.connection.path = '/xyz/twe/yub'
        self.known_task.connection.save()
        manager = FTPManager(self.known_task, self.PATH)
        manager.run()
        self.assertFalse(manager.status)

    @patch('remote_connection.connection_manager.ftp_manager.FTP')
    def test_change_permission_error(self, MockFTP):
        """
            Validates storbinary method
        """
        MockFTP.return_value = Mock()
        MockFTP.return_value.storbinary.side_effect = Exception
        MockFTP.return_value.voidcmd.return_value = 'None'
        MockFTP.return_value.size.return_value = 3

        manager = FTPManager(self.known_task, self.PATH)
        manager.connect()
        self.assertRaises(ExceptionPermissionError, manager.upload_file)

