import os

from django.test import TestCase

from task.exporter import Export
from task.exporter import ExportRuntimeException


class ExportTests(TestCase):
    SUCCESS_QUERY = 'SELECT * FROM django_content_type'
    FAIL_QUERY = 'SELECT * FROM not_exist_yet'

    def test_syntax_success(self):
        """ Run the query successfully """
        self.assertEqual(True, Export.syntax_checker(self.SUCCESS_QUERY))

    def test_syntax_fail_table_does_not_exist(self):
        """ The method will return false because of the table does not exist """
        self.assertEqual(False, Export.syntax_checker(self.FAIL_QUERY))

    def test_syntax_fail_syntax_error(self):
        """ The query has syntax error """
        self.assertEqual(False, Export.syntax_checker('SELECT *id FROM django_content_type'))

    def test_query_exporter_success(self):
        """ The method will save the file under the /dump folder and will return the path """
        path = Export.query_exporter(self.SUCCESS_QUERY)
        self.assertEqual(True, len(path) > 10)
        self.assertEqual(True, os.path.exists(path))
        os.unlink(path)

    def test_query_exporter_fail(self):
        """ Exporter will raise RunTimeException due to query syntax"""
        self.assertRaises(ExportRuntimeException, Export.query_exporter, 'ASC')
