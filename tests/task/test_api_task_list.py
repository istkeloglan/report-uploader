from django.urls import reverse
from mock import patch, Mock
from rest_framework import status
from rest_framework.test import APITestCase
from django.conf import settings
from task.factories import PeriodicTaskFactory
from task.factories import RemoteConnectionFactory
from task.factories import ScheduleFactory
from task.factories import TaskFactory
from task.factories import TaskLogFactory
from task.models import Task
from task.tasks import run
import os
import glob


class ApiTaskListTests(APITestCase):
    def setUp(self):
        self.task_url = reverse('tasks')

    def __create_dummy_data(self):
        self.conn = RemoteConnectionFactory()
        self.schedule = ScheduleFactory()
        self.pschedule = PeriodicTaskFactory(
            crontab=self.schedule
        )
        self.task = TaskFactory(
            connection=self.conn,
            periodic_task=self.pschedule,
        )
        TaskLogFactory(task=self.task)
        TaskLogFactory(task=self.task)
        TaskLogFactory(task=self.task)
        TaskLogFactory(task=self.task)
        return self.conn, self.schedule, self.pschedule, self.task

    def __check_request(self, url):
        log_response = self.client.get(url, format='json')
        self.assertEqual(log_response.status_code, status.HTTP_200_OK)
        return log_response.json()

    def test_result_list(self):
        """
        Should be get list of tasks/task_logs from api
        """

        # create the dummy data
        Task.objects.all().delete()
        conn, schedule, pschedule, task = self.__create_dummy_data()

        # Call the end-point
        response = self.__check_request(self.task_url)
        self.assertNotEqual(len(response['results']), 0)

        # Validate the output of the list api
        obj = response['results'][0]

        # Validate the task object
        self.assertEqual(obj['id'], task.id)
        self.assertIsNotNone(obj['name'])
        self.assertIsNotNone(obj['description'])
        self.assertIsNotNone(obj['created_at'])

        # Call log end-point
        log_response = self.__check_request(
            reverse('logs', kwargs={'task_id': task.id})
        )
        # Validate the size of results
        self.assertEqual(len(log_response['results']), 4)

        # Get the first object
        task_log = log_response['results'][0]
        # Validate the details
        self.assertIsNotNone(task_log['log'])
        self.assertIsNotNone(task_log['created_at'])
        self.assertIsNotNone(task_log['status'])

        # Get task details
        task_details = self.__check_request(
            reverse('task-detail', kwargs={'pk': task.id})
        )

        # Validate the details details
        # base task object
        self.assertEqual(task_details['id'], task.id)
        self.assertEqual(task_details['name'], task.name)
        self.assertEqual(task_details['description'], task.description)
        self.assertEqual(task_details['query'], task.query)

        # schedule object
        self.assertEqual(task_details['schedule']['minute'], str(task.periodic_task.crontab.minute))
        self.assertEqual(task_details['schedule']['hour'], str(task.periodic_task.crontab.hour))
        self.assertEqual(task_details['schedule']['day_of_week'], str(task.periodic_task.crontab.day_of_week))
        self.assertEqual(task_details['schedule']['day_of_month'], str(task.periodic_task.crontab.day_of_month))
        self.assertEqual(task_details['schedule']['month_of_year'], str(task.periodic_task.crontab.month_of_year))

        # connection object
        self.assertEqual(task_details['connection']['port'], task.connection.port)
        self.assertEqual(task_details['connection']['username'], task.connection.username)
        self.assertEqual(task_details['connection']['host'], task.connection.host)
        self.assertEqual(task_details['connection']['path'], task.connection.path)
        self.assertEqual(task_details['connection']['connection_type'], task.connection.connection_type)
        self.assertEqual(task_details['connection']['timeout'], task.connection.timeout)

        # run the task as an async job
        log_response = self.client.post(reverse('task-detail', kwargs={'pk': task.id}), format='json')
        self.assertEqual(log_response.status_code, status.HTTP_200_OK)

    @patch('remote_connection.connection_manager.ftp_manager.FTP')
    def test_create_new_task_success(self, MockFTP):
        """ Validate the create_new_task end-point step by step """
        # mock for FTP Class
        MockFTP.return_value = Mock()
        MockFTP.return_value.pwd.return_value = '/23'
        MockFTP.return_value.voidcmd.return_value = 'None'
        MockFTP.return_value.size.return_value = 3
        raw_data = {}

        # needs to raise 400 error due to empty data
        response = self.client.post(self.task_url, raw_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # port number out of range
        raw_data = {
            'name': 'name',
            'description': 'description',
            'query': 'SELECT * FROM auth_permission',
            'schedule_minute': '4',
            'schedule_hour': '*',
            'schedule_day_of_week': '*',
            'schedule_day_of_month': '*',
            'schedule_month_of_year': '0',
            'port': 100000,
            'host': 'host',
            'username': 'username',
            'password': 'pass',
            'path': '/remote/path',
            'connection_type': 1,
            'timeout': 30,
        }
        response = self.client.post(self.task_url, raw_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # the query is invalid
        raw_data['query'] = 'SELECT-FROM auth_permission'
        response = self.client.post(self.task_url, raw_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # the schedule parameter is incorrect
        raw_data['schedule_day_of_week'] = 'T'
        response = self.client.post(self.task_url, raw_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # fix the broken params
        raw_data['port'] = 21
        raw_data['query'] = 'SELECT * FROM auth_permission'
        raw_data['schedule_day_of_week'] = '*'

        # status code needs to be 201/created
        response = self.client.post(self.task_url, raw_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # since we have a task, we can trigger the background job
        task = response.json()
        try:
            run(task_id=task['id'])
        except:  # pragma: no cover msg = "no need to check"
            self.fail('Task failed')

        # If the manager raises any exception, we should retry the task
        # and increase the number_of_tries value
        MockFTP.return_value.login.side_effect = Exception
        self.assertRaises(Exception, run, task_id=task['id'])
        t = Task.objects.get(id=task['id'])
        self.assertEqual(1, t.connection.number_of_tries)

        # If we set the settings value to 1, the logic needs to disabled the task
        settings.TASK_MAX_NUMBER_OF_TRIES = 1

        run(task_id=task['id'])
        t = Task.objects.get(id=task['id'])
        self.assertFalse(t.periodic_task.enabled)

        # If we had incorrect task_id, the app still works but returns False
        try:
            run(task_id=None)
        except:  # pragma: no cover msg = "no need to check"
            self.fail('Task failed')

    def test_create_new_task_fail(self):
        """ Call the end-point with incorrect credentials """
        raw_data = {
            'name': 'name',
            'description': 'description',
            'query': 'SELECT * FROM auth_permission',
            'schedule_minute': '4',
            'schedule_hour': '*',
            'schedule_day_of_week': '*',
            'schedule_day_of_month': '*',
            'schedule_month_of_year': '0',
            'port': 22,
            'host': 'host',
            'username': 'username',
            'password': 'pass',
            'path': '/remote/path',
            'connection_type': 1,
            'timeout': 1,
        }
        response = self.client.post(self.task_url, raw_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
