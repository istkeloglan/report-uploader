# Data Uploader
- http://192.241.215.18:8000/

# Requirements
- Redis Server (4.0)

# Installation

`git clone https://istkeloglan@bitbucket.org/istkeloglan/report-uploader.git`

`cd report-uploader`


## Front-end deployment
##### This step is optional. The repo already contains the list of files

`cd frontend`

Download nodejs if you don't have one : https://nodejs.org/en/download/

`npm install`

`npm run build`


## Virtualenv

Create virtualenv

`virtualenv -p python3.5 venv`

Activate the environment

`source venv/bin/activate`

Install the packages

`pip install -r requirements.pip`

## Settings
#### Redis and Database parameters


`CELERY_BROKER_URL = 'redis://localhost:6379/0'`

`CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'`

`DATABASES` 

## Celery workers and Django-server
#### In the same folder, run the following commands:

`python manage.py migrate`

`python manage.py collectstatic --noinput`

`python manage.py runscript generate_data` 
###### Generate dummy date / each run generates 1000 report objects

`mkdir dump`
###### temp. file folder. 

`python manage.py runserver 0.0.0.0:8000`

`celery -A lyft worker -l info`
`celery -A lyft beat -l debug --scheduler=lyft.job_updater.AutoUpdateScheduler`


## Test Coverage: 100%
`coverage run ./manage.py test tests`
`coverage html`
Please see htmlcov/index.html


## Example SQL Queries
`SELECT * FROM reports_reports`

`SELECT * FROM task`

`SELECT * FROM auth_permission`

`SELECT * FROM django_content_type`


## SFTP Server Credentials
`user: root`

`pass: lyft321`

`ip: 167.99.7.95`

`port:22`

`path:/lyft-1`
 
`path:/lyft-2`

`path:/lyft-3`

`path:/lyft-4`

`path:/lyft-5`
`path:/lyft-6`

`path:/lyft-7`