import csv
import uuid

from django.conf import settings
from django.db import connection


class Export:
    @staticmethod
    def syntax_checker(query):
        cursor = connection.cursor()
        try:
            cursor.execute('EXPLAIN {}'.format(query))
            return True
        except:
            return False

    @staticmethod
    def query_exporter(query):
        try:
            cursor = connection.cursor()
            cursor.execute(query)
            path = "{path}{file}.csv".format(
                path=settings.CSV_EXPORT_PATH,
                file=str(uuid.uuid4())
            )
            csv_writer = csv.writer(open(path, "wt"), delimiter=',')

            # Copy the headers
            csv_writer.writerow([i[0] for i in cursor.description])
            # Copy the rows
            csv_writer.writerows(cursor)
            del csv_writer
            return path
        except:
            raise ExportRuntimeException


class ExportRuntimeException(Exception):
    """ Runtime error. The issue is the file system or query syntax """

