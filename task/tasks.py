from django.conf import settings

from core.notification import Notification
from lyft.celery import app
from remote_connection.connection_manager.ftp_manager import FTPManager
from remote_connection.connection_manager.sftp_manager import SFTPManager
from task.exporter import Export
from task.models import Task


@app.task(
    retry_kwargs={'max_retries': settings.CELERY_NUMBER_OF_TRIES},
    default_retry_delay=settings.CELERY_RETRY_DELAY,
)
def run(task_id):
    """
    Runs the RemoteConnectionManager in the background
    :param task_id: Task Object pk
    :return:
    """
    try:
        task = Task.objects.get(id=task_id, periodic_task__enabled=True)
    except Task.DoesNotExist:
        return False
    list_of_managers = (None, FTPManager, SFTPManager)
    conn = None

    # Check the export output
    try:
        export = Export.query_exporter(task.query)
    except:
        export = None

    try:
        conn = list_of_managers[task.connection.connection_type](
            task,
            export
        )
        conn.run()
    except:

        if export is None:
            conn.log(False, 'Query is not valid')
        conn.garbage_collection()

        if task.connection.number_of_tries >= settings.TASK_MAX_NUMBER_OF_TRIES:
            task.periodic_task.enabled = False
            task.periodic_task.save()
        else:
            task.connection.number_of_tries += 1
            task.connection.save()
            raise Exception()

        # We might have some notification / 3rd party integrations
        Notification.notify('Task {} failed.')
