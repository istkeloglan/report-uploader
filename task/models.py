from django.db import models
from django_celery_beat.models import PeriodicTask
from remote_connection.models import RemoteConnection


class Task(models.Model):
    class Meta:
        db_table = 'task'

    name = models.CharField(null=False, max_length=255)
    description = models.CharField(null=True, max_length=255)
    query = models.TextField(null=False)
    connection = models.ForeignKey(RemoteConnection, on_delete=None, null=False)
    periodic_task = models.ForeignKey(PeriodicTask, on_delete=models.CASCADE, null=False, related_name='celery_periodic_task')
    created_at = models.DateTimeField(auto_now=True)


class TaskLog(models.Model):
    class Meta:
        db_table = 'task_log'

    task = models.ForeignKey(Task, on_delete=models.CASCADE, null=False)
    log = models.TextField(null=False)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True)
