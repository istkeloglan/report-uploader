import uuid
import factory
import arrow
from .models import Task, TaskLog
from random import randint

from remote_connection.factories import RemoteConnectionFactory
from django_celery_beat.models import CrontabSchedule
from django_celery_beat.models import PeriodicTask


def generate_unique_name():
    return str(uuid.uuid4()) + str(randint(0, 99999999))


class ScheduleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CrontabSchedule

    minute = randint(0, 60)
    hour = randint(0, 60)
    day_of_week = randint(0, 6)
    day_of_month = randint(0, 28)
    month_of_year = randint(0, 6)


class PeriodicTaskFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PeriodicTask

    name = generate_unique_name()
    task = generate_unique_name()
    crontab = ScheduleFactory()
    expires = arrow.utcnow().shift(minutes=+4).datetime


class TaskFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Task

    name = generate_unique_name()
    description = generate_unique_name()
    query = generate_unique_name()
    connection = RemoteConnectionFactory()
    periodic_task = PeriodicTaskFactory()


class TaskLogFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TaskLog

    log = generate_unique_name()
