from rest_framework import generics
from rest_framework.permissions import AllowAny

from task.api.serializers import DetailTaskLogSerializer
from task.models import TaskLog


class TaskLogViewSet(generics.ListAPIView):
    """
    A viewset to list the logs of the task
    """
    lookup_field = 'task_id'
    permission_classes = (AllowAny,)
    queryset = TaskLog.objects.all()
    serializer_class = DetailTaskLogSerializer

    def get_queryset(self):
        id = self.kwargs.get(self.lookup_field)
        comments = TaskLog.objects.order_by('-id').filter(task_id=id)
        return comments
