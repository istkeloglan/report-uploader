from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from task.api.serializers import DetailTaskSerializer
from task.models import Task
from task.tasks import run


class TaskDetailViewSet(
    generics.RetrieveAPIView,
    generics.CreateAPIView,
):
    """
    A viewset to serve the task details
    """
    RUNNING = 1
    permission_classes = (AllowAny,)
    queryset = Task.objects.prefetch_related('periodic_task').all()
    serializer_class = DetailTaskSerializer

    def post(self, request, pk=None):
        obj = self.get_object()
        run.delay(obj.id)
        return Response({'status': self.RUNNING})
