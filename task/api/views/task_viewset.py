from rest_framework import generics
from rest_framework.permissions import AllowAny
from task.api.serializers import CreateTaskSerializer
from task.api.serializers import ListTaskSerializer
from task.models import Task


class TaskViewSet(
    generics.CreateAPIView,
    generics.ListAPIView,
    generics.RetrieveAPIView
):
    """
    A viewset to serve the task actions
    """
    permission_classes = (AllowAny,)
    queryset = Task.objects.order_by('-id').all()

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return CreateTaskSerializer
        return ListTaskSerializer
