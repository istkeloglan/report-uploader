from django.conf import settings
from rest_framework import serializers

from task.models import TaskLog


class DetailTaskLogSerializer(serializers.ModelSerializer):
    log = serializers.CharField(read_only=True)
    status = serializers.BooleanField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True, format=settings.DATETIME_FORMAT, default_timezone=settings.TIME_ZONE)

    class Meta:
        model = TaskLog
        fields = ['id', 'log', 'created_at', 'status']
