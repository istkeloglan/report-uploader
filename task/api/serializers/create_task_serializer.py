import json
import uuid

from django.db import transaction
from django_celery_beat.models import CrontabSchedule, PeriodicTask
from rest_framework import serializers
from django.conf import settings
from core.api import ValidationError
from remote_connection.connection_manager.ftp_manager import FTPManager
from remote_connection.connection_manager.sftp_manager import SFTPManager
from remote_connection.models import RemoteConnection
from task.exporter import Export
from task.models import Task
from core.crypt import Crypt


class CreateTaskSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(required=True)
    description = serializers.CharField(required=True)
    query = serializers.CharField(required=True)

    connection_id = serializers.IntegerField(required=False, write_only=True, default=None)
    port = serializers.IntegerField(required=False, write_only=True)
    timeout = serializers.IntegerField(required=False, write_only=True)
    host = serializers.CharField(required=False, write_only=True)
    username = serializers.CharField(required=False, write_only=True)
    password = serializers.CharField(required=False, write_only=True)
    path = serializers.CharField(required=False, write_only=True)
    connection_type = serializers.IntegerField(required=False, write_only=True)

    schedule_minute = serializers.ChoiceField(required=True, choices=list(range(0, 61)) + ["*"], write_only=True)
    schedule_hour = serializers.ChoiceField(required=True, choices=list(range(0, 25)) + ["*"], write_only=True)
    schedule_day_of_week = serializers.ChoiceField(required=True, choices=list(range(0, 7)) + ["*"], write_only=True)
    schedule_day_of_month = serializers.ChoiceField(required=True, choices=list(range(0, 32)) + ["*"], write_only=True)
    schedule_month_of_year = serializers.ChoiceField(required=True, choices=list(range(0, 13)) + ["*"], write_only=True)

    class Meta:
        model = Task
        fields = [
            'id', 'name', 'description', 'query',
            'port', 'host', 'username',
            'password', 'path', 'timeout',
            'connection_id',
            'connection_type',
            'schedule_minute',
            'schedule_hour',
            'schedule_day_of_week',
            'schedule_day_of_month',
            'schedule_month_of_year'
        ]

    def validate(self, data):
        if data['port'] not in list(range(1, 65535)):
            raise ValidationError('The port number should be between 1 and 65535')
        list_of_managers = (None, FTPManager, SFTPManager)
        try:
            list_of_managers[data['connection_type']]().get_connection(
                host=data['host'],
                timeout=data['timeout'],
                user=data['username'],
                password=data['password'],
                port=data['port']
            )
        except Exception:
            raise ValidationError('The connection could not be established')
        return data

    def validate_query(self, value):
        """ Validates that the query is runnable """
        if not Export.syntax_checker(value):
            raise ValidationError('Query is not valid')
        return value

    def create(self, data):
        with transaction.atomic():
            crypt = Crypt()
            connection, _ = RemoteConnection.objects.get_or_create(
                port=data['port'],
                host=data['host'],
                username=crypt.encrypt(data['username']),
                password=crypt.encrypt(data['password']),
                path=data['path'],
                connection_type=data['connection_type'],
            )
            # Create the schedule
            schedule, _ = CrontabSchedule.objects.get_or_create(
                minute=data['schedule_minute'],
                hour=data['schedule_hour'],
                day_of_week=data['schedule_day_of_week'],
                day_of_month=data['schedule_day_of_month'],
                month_of_year=data['schedule_month_of_year'],
            )
            # Create the background job
            pt = PeriodicTask.objects.create(
                crontab=schedule,
                name=str(uuid.uuid4()),
                task=settings.BACKGROUND_TASK,
            )
            # Create the task object
            task = Task.objects.create(
                name=data['name'],
                description=data['description'],
                query=data['query'],
                connection_id=connection.id,
                periodic_task_id=pt.id,
            )
            # Assign the task_id to celery task.
            pt.args = json.dumps([task.id])
            pt.save()
            return task
