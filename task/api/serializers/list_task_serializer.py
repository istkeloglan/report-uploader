from django.conf import settings
from rest_framework import serializers

from task.models import Task


class ListTaskSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)
    query = serializers.CharField(read_only=True)
    enabled = serializers.SerializerMethodField()
    created_at = serializers.DateTimeField(read_only=True, format=settings.DATETIME_FORMAT, default_timezone=settings.TIME_ZONE)

    class Meta:
        model = Task
        fields = [
            'id', 'name', 'description', 'query', 'created_at', 'enabled'
        ]

    def get_enabled(self, obj):
        return obj.periodic_task.enabled
