from django.conf import settings
from rest_framework import serializers

from task.models import Task
from task.api.serializers.detail_crontab_serializer import DetailCrontabSerializer
from remote_connection.api.serializers import DetailRemoteConnectionSerializer


class DetailTaskSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)
    query = serializers.CharField(read_only=True)
    schedule = serializers.SerializerMethodField(read_only=True)
    connection = serializers.SerializerMethodField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True, format=settings.DATETIME_FORMAT, default_timezone=settings.TIME_ZONE)

    class Meta:
        model = Task
        fields = ['id', 'name', 'description', 'query', 'schedule', 'created_at', 'connection']

    def get_schedule(self, obj):
        return DetailCrontabSerializer(obj.periodic_task.crontab).data

    def get_connection(self, obj):
        return DetailRemoteConnectionSerializer(obj.connection).data
