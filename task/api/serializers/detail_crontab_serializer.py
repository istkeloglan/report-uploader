from django_celery_beat.models import CrontabSchedule
from rest_framework import serializers


class DetailCrontabSerializer(serializers.ModelSerializer):
    minute = serializers.CharField(read_only=True)
    hour = serializers.CharField(read_only=True)
    day_of_week = serializers.CharField(read_only=True)
    day_of_month = serializers.CharField(read_only=True)
    month_of_year = serializers.CharField(read_only=True)

    class Meta:
        model = CrontabSchedule
        fields = [
            'minute', 'hour', 'day_of_week', 'day_of_month', 'month_of_year',
        ]
