import React from 'react';
import { connect } from 'react-redux';

import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import { Card, CardActions, CardTitle } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import axios from 'axios';
import { push } from 'react-router-redux';
import { NavLink } from 'react-router-dom';

export const ClickableRow = (props) => {
  const { rowData, ...restProps } = props;
  return (
    <TableRow
      {...restProps}
      onMouseDown={() => console.log('clicked', restProps.redirect(rowData))}
    >
      {props.children}
    </TableRow>
  );
};

class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      records: [],
      loading: true,
      error: false,
    };
  }

  redirect(id) {
    this.props.dispatch(push(`/tasks/${id}`));
  }

  redirectToNewTask() {
    this.props.dispatch(push('/form'));
  }

  componentWillMount() {
    axios.get('/api/tasks/').then((r) => {
      this.setState({ records: r.data.results, loading: false, error: false });
    }).catch((e) => {
      this.setState({ records: [], loading: false, error: true });
    });

    this.redirect = this.redirect.bind(this);
    this.redirectToNewTask = this.redirectToNewTask.bind(this);
  }

  getTable() {
    return (<div>
      {this.state.records.length < 1 ? <div style={{paddingLeft: 30}} >There is no active task</div> :
      <Table selectable={false} showRowHover={true} >
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow displaySelectAll={false}>
            <TableHeaderColumn>ID</TableHeaderColumn>
            <TableHeaderColumn>Name</TableHeaderColumn>
            <TableHeaderColumn>Description</TableHeaderColumn>
            <TableHeaderColumn>Enabled</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false} showRowHover={true}>
          {this.state.records.map((result) =>
            (<ClickableRow rowData={result.id} key={result.id} redirect={this.redirect} >
              <TableRowColumn>{result.id}</TableRowColumn>
              <TableRowColumn>{result.name}</TableRowColumn>
              <TableRowColumn>{result.description}</TableRowColumn>
              <TableRowColumn>{result.enabled ? 'Enabled' : 'Disabled'}</TableRowColumn>
            </ClickableRow>)
          )}
        </TableBody>
      </Table> }
    </div>);
  }

  render() {
    const style = {
      container: {
        position: 'relative',
      },
    };
    return (
      <div>
        {this.state.error ? <div style={{ textAlign: 'center' }} >
          <i className="material-icons" >error</i> Http error accured
        </div> : <Card style={{ paddingBottom: 30 }} >
          <CardTitle title="Job Scheduler" />
          <div style={{ position: 'relative' }} >
            <CardActions style={{ position: 'absolute', right: 0, marginTop: -60 }} >
              <NavLink to="/form">
                <FlatButton label="Add New Task" onClick={this.redirectToNewTask} />
              </NavLink>
            </CardActions>
          </div>
          {this.state.loading ? <RefreshIndicator
            size={40}
            left={10}
            top={0}
            status="loading"
            style={style.refresh}
          /> : this.getTable()}
        </Card> }
      </div>

    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(null, mapDispatchToProps)(HomePage);
