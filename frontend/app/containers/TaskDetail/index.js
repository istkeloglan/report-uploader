/**
 *
 * TaskDetail
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import axios from 'axios';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import _ from 'lodash';
import { push } from 'react-router-redux';

import { Card, CardHeader, CardTitle } from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';

export class TaskDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      logs: [],
      loading: true,
      open: false,
      log_detail: '',
      running: false,
    };
  }

  handleOpen = (log) => {
    this.setState({open: true, log_detail: log});
  };

  handleClose = () => {
    this.setState({open: false, log_detail: null});
  };

  loadLogData = () => {
    axios.get(`/api/log/${this.props.match.params.taskId}/`).then((r) => {
      this.setState({
        logs: r.data.results,
      });
    });
  };

  componentWillMount() {
    axios.get(`/api/tasks/${this.props.match.params.taskId}/`).then((r) => {
      this.setState({
        data: r.data,
        loading: false,
      });
    }).catch((e) => {
      this.setState({
        loading: true,
      });
    });
    this.loadLogData();
  }

  runInBackground = () => {
    axios.post(`/api/tasks/${this.props.match.params.taskId}/`).then((r) => {
      this.setState({
        running: true,
      });
    });
    setTimeout(() => {
      this.setState({running: false});
      this.loadLogData();
    }, 5000);
  };

  redirectToHome() {
    this.props.dispatch(push('/'));
  }

  render() {
    const data = _.get(this.state, 'data', null);
    const actions = [
      <FlatButton
        label="Done"
        primary={true}
        onClick={this.handleClose}
      />,
    ];
    return (
      <div>

        <RaisedButton
          label="< Back to results"
          secondary={true}
          style={{position: 'absolute', left: 0, top: 0}}
          onClick={this.redirectToHome.bind(this)}
        />
        {this.state.loading ? <RefreshIndicator
          size={40}
          left={10}
          top={0}
          status="loading"
        /> : <div>
          <Card>
            <div className="row" >
              <div className="col-xs-9" >
                <div className="box" >
                  <CardTitle title={data.name} subtitle={data.description} />
                </div>
              </div>
              <div className="col-xs-3" >
                <div className="box" style={{textAlign: 'right'}} >
                  <RaisedButton
                    style={{margin: 20}}
                    label={this.state.running ? 'Running' : 'Run In Background'}
                    disabled={this.state.running}
                    primary={true}
                    onClick={this.runInBackground}
                  />
                </div>
              </div>
            </div>
            <Divider />
            <CardHeader title='Query' subtitle={data.query} />
            <Divider />
            <div className="row" >
              <div className="col-xs-6" >
                <div className="box" >
                  <CardHeader
                    title='Connection'
                    subtitle={`*****@${data.connection.host}:${data.connection.port}/${data.connection.path} Timeout: ${data.connection.timeout}`}
                  />
                </div>
              </div>
              <div className="col-xs-4" >
                <div className="box" >
                  <CardHeader
                    title='Created At'
                    subtitle={data.created_at}
                  />
                </div>
              </div>
              <div className="col-xs-2" >
                <div className="box" >
                  <CardHeader
                    title='Schedule'
                    subtitle={`${data.schedule.minute} ${data.schedule.hour} ${data.schedule.day_of_week} ${data.schedule.day_of_month} ${data.schedule.month_of_year}`}
                  />
                </div>
              </div>
            </div>
          </Card>
          <Card>
            <Dialog
              title="Log Details"
              actions={actions}
              modal={true}
              open={this.state.open}
              autoScrollBodyContent={true}
            >
              <pre>{_.get(this.state, 'log_detail', '')}</pre>
            </Dialog>
            {this.state.logs.length > 0 ? <div>
              <CardTitle subtitle='Task Logs (Last 50 records)' />
              <Divider />
              {this.state.logs.map((r) => {
                return (<div key={r.id} onClick={this.handleOpen.bind(this, r.log)} >
                  <CardHeader title={r.status ? 'Success' : 'Failure'} subtitle={r.created_at} />
                  <Divider />
                </div>);
              })}
            </div>
              : ''}
          </Card>
        </div>}
      </div>
    );
  }
}

TaskDetail.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(TaskDetail);
