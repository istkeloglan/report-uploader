/**
 *
 * Form
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import NewSchedule from './newSchedule';
import axios from 'axios';
import { Card, CardTitle } from 'material-ui/Card';
import _ from 'lodash';
import { push } from 'react-router-redux';

export class Form extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentWillMount() {
    this.state = {
      error: null,
      loading: false,
    };
  }

  handleFormSubmit = (data) => {
    this.setState({loading: true, error: null});
    axios.post('/api/tasks/', data).then((r) => {
      this.props.dispatch(push('/'));
    }).catch((e) => {
      const keys = Object.keys(e.response.data);
      let err = '';
      if (keys.indexOf('detail') !== -1) {
        err = e.response.data['detail'];
      } else {
        err = e.response.data[Object.keys(e.response.data)[0]][0];
      }
      this.setState({
        error: err,
        loading: false,
      });
    });
  };

  render() {
    return (
      <Card>
        <CardTitle
          title="New Schedule"
          subtitle={this.state.error}
          subtitleColor="red"
        />
        <NewSchedule onSubmit={this.handleFormSubmit} loading={this.state.loading} />
      </Card>
    );
  }
}

Form.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(Form);
