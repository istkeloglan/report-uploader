import React from 'react';
import { Field, reduxForm } from 'redux-form/immutable';
import { renderField, renderFieldTextArea, renderRadioGroup } from './fieldRender';
import RaisedButton from 'material-ui/RaisedButton';
import Subheader from 'material-ui/Subheader';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import { NavLink } from 'react-router-dom'

const required = value => (value ? undefined : 'Required');


const NewSchedule = (props) => {
  const {handleSubmit} = props;
  return (<div style={{padding: 20}} >
      <form onSubmit={handleSubmit} >
        <Subheader>Task Details</Subheader>
        <div className="row input_field_wrp" >
          <div className="col-xs-12" >
            <div className="box" >
              <Field name="name" type="text" component={renderField} label="Job Title" validate={[required]} />
            </div>
          </div>
        </div>
        <div className="row input_field_wrp" >
          <div className="col-xs-12" >
            <div className="box" >
              <Field name="description" type="text" component={renderFieldTextArea} label="Job Description"
                     validate={[required]} />
            </div>
          </div>
        </div>
        <div className="row input_field_wrp" >
          <div className="col-xs-12" >
            <div className="box" >
              <Field name="query" type="text" component={renderFieldTextArea} label="SQL Query" validate={[required]} />
            </div>
          </div>
        </div>
        <Subheader>Schedule</Subheader>
        <div className="row input_field_wrp" >
          <div className="col-xs-2" >
            <div className="box" >
              <div style={{margin: 10}} >Schedule</div>
            </div>
          </div>
          <div className="col-xs-2" >
            <div className="box" >
              <Field name="schedule_minute" type="text" maxLenght="1" component={renderField} label="*"
                     validate={[required]} />
            </div>
          </div>
          <div className="col-xs-2" >
            <div className="box" >
              <Field name="schedule_hour" type="text" component={renderField} label="*" validate={[required]} />
            </div>
          </div>
          <div className="col-xs-2" >
            <div className="box" >
              <Field name="schedule_day_of_week" type="text" component={renderField} label="*" validate={[required]} />
            </div>
          </div>
          <div className="col-xs-2" >
            <div className="box" >
              <Field name="schedule_day_of_month" type="text" component={renderField} label="*" validate={[required]} />
            </div>
          </div>
          <div className="col-xs-2" >
            <div className="box" >
              <Field name="schedule_month_of_year" type="text" component={renderField} label="*"
                     validate={[required]} />
            </div>
          </div>
        </div>
        <Subheader>Connection Details</Subheader>
        <div className="row input_field_wrp" >
          <div className="col-lg-12" >
            <Field name="connection_type" component={renderRadioGroup} validate={[required]} >
              <RadioButton value="1" label="FTP" />
              <RadioButton value="2" label="SFTP" />
            </Field>
          </div>
        </div>
        <div className="row input_field_wrp" >
          <div className="col-xs-6" >
            <div className="box" >
              <Field name="username" type="text" component={renderField} label="Username" validate={[required]} />
            </div>
          </div>
          <div className="col-xs-6" >
            <div className="box" >
              <Field name="password" type="text" component={renderField} label="Password" validate={[required]} />
            </div>
          </div>
        </div>
        <div className="row input_field_wrp" >
          <div className="col-xs-5" >
            <div className="box" >
              <Field name="host" type="text" component={renderField} label="Host" validate={[required]} />
            </div>
          </div>
          <div className="col-xs-1" >
            <div className="box" >
              <Field name="port" type="text" component={renderField} label="Port" validate={[required]} />
            </div>
          </div>
          <div className="col-xs-2" >
            <div className="box" >
              <Field name="timeout" type="text" component={renderField} label="Timeout" validate={[required]} />
            </div>
          </div>
          <div className="col-xs-4" >
            <div className="box" >
              <Field name="path" type="text" component={renderField} label="Path" validate={[required]} />
            </div>
          </div>
        </div>
        <div className="row" >
          <div className="col-lg align-right" >
            <RaisedButton label="Submit" primary={true} type="submit" disabled={props.loading} />
            <NavLink to="/"><RaisedButton label="Cancel" style={{marginLeft: 20}} primary={false} /></NavLink>
          </div>
        </div>
      </form>
    </div>
  );
};

export default reduxForm({
  form: 'new_schedule',
  initialValues: {connection_type: '1'}
})(NewSchedule);
