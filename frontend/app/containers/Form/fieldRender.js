import React from 'react';
import TextField from 'material-ui/TextField';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

export const renderField = ({input, label, type, meta: {touched, error}}) => {
  return (<TextField {...input} type={type} placeholder={label} fullWidth={true} errorText={(touched && error) ? error : null} />);
};
export const renderFieldTextArea = ({input, label, type, meta: {touched, error}}) => {
  return (<TextField {...input} type={type} placeholder={label} fullWidth={true} errorText={(touched && error) ? error : null} multiLine={true} rows={2} rowsMax={4} />);
};

export const renderRadioGroup = ({ input, ...rest }) => (
  <RadioButtonGroup
    {...input}
    {...rest}
    valueSelected={input.value}
    onChange={(event, value) => input.onChange(value)}
  />
)


