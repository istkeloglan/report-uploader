import base64
from django.conf import settings
from Crypto.Cipher import XOR


class Crypt:
    def __init__(self):
        # must be ENV value
        self.secret_key = settings.CRYPT_KEY

    def encrypt(self, plaintext):
        cipher = XOR.new(self.secret_key)
        return base64.b64encode(cipher.encrypt(plaintext)).decode('utf-8')

    def decrypt(self, ciphertext):
        cipher = XOR.new(self.secret_key)
        return cipher.decrypt(base64.b64decode(ciphertext)).decode('utf-8')
