from django.db import models


class Reports(models.Model):
    campaign_id = models.BigIntegerField(default=0)
    adgroup_id = models.BigIntegerField(default=0)
    name = models.CharField(max_length=255)
    cost = models.DecimalField(default=0, decimal_places=3, max_digits=30)
    views = models.PositiveIntegerField(default=0)
    clicks = models.PositiveIntegerField(default=0)
    impressions = models.PositiveIntegerField(default=0)
    conversions = models.IntegerField(default=0)
    report_date = models.DateField(auto_now=False, db_index=True, null=True)
