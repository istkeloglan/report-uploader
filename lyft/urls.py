from django.conf.urls import url
from django.urls import re_path
from django.views.generic import TemplateView

from task.api.views import TaskDetailViewSet
from task.api.views import TaskLogViewSet
from task.api.views import TaskViewSet

urlpatterns = [
    url(r'api/tasks/(?P<pk>[0-9]+)/$', TaskDetailViewSet.as_view(), name='task-detail'),
    url(r'api/tasks/', TaskViewSet.as_view(), name='tasks'),
    url(r'api/log/(?P<task_id>[0-9]+)/$', TaskLogViewSet.as_view(), name='logs'),

    # everything else goes to index
    re_path('^$', TemplateView.as_view(template_name='index.html')),
]

