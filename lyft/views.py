from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from django.views.decorators.csrf import requires_csrf_token
import os


@requires_csrf_token
def handler404(request, exception, template_name=None):
    if os.path.exists(request.path[1:]) and request.path[-3:] in ['ico', '.js', '.css']:
        with open(request.path[1:], 'rt') as file:
            return HttpResponse(file.readlines())
    return render(request, 'index.html', status=400)
