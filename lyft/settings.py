import os

from django.views import defaults
from lyft.views import handler404

# All the parameters must migrate to ENV

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'f5g$b*3iy%appr((6nnpu!^aou^_(+&&0jp*z^&hr^#2s_9^s#'
DEBUG = False

# These parameters need to change once the app has a domain / static IP
ALLOWED_HOSTS = ['*']
CORS_ORIGIN_WHITELIST = ['*']
CORS_ORIGIN_ALLOW_ALL = True

CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'
IS_TESTING = False
BACKGROUND_TASK = 'task.tasks.run'
ROOT_URLCONF = 'lyft.urls'
WSGI_APPLICATION = 'lyft.wsgi.application'
DATABASE_NAME = 'lyft'
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'America/Los_Angeles'
USE_I18N = True
USE_L10N = False
USE_TZ = True
STATIC_URL = '/static/'
STATIC_ROOT = 'static/'
CSV_EXPORT_PATH = '{}/dump/'.format(BASE_DIR)
DATETIME_FORMAT = '%Y/%m/%d %H:%M:%S'
defaults.page_not_found = handler404
TASK_MAX_NUMBER_OF_TRIES = 30
CELERY_NUMBER_OF_TRIES = 3
CELERY_RETRY_DELAY = 60 * 60
CRYPT_KEY = 'd41d8cd98f00b204e9800998ecf8427e'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'celery',
    'django_celery_beat',
    'django_extensions',
    'rest_framework',
    'corsheaders',
    'remote_connection',
    'task',
    'reports',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            './frontend/build'
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'lyft',
        'USER': 'root',
        'PASSWORD': 'lyft321',
        'HOST': 'localhost',
        'PORT': 3306,
    }
}

STATICFILES_DIRS = [
    './frontend/build',
]
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 50
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'CRITICAL'),
        },
        'celery': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'CRITICAL'),
        },
    },
}
