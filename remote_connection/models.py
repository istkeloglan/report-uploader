from django.db import models


class RemoteConnection(models.Model):
    class Meta:
        db_table = 'connection'

    TYPE_FTP = 1
    TYPE_SFTP = 2
    TYPE_DROPBOX = 3
    TYPE_GOOGLE_DRIVE = 4

    TYPE_CHOICES = (
        (TYPE_FTP, 1),
        (TYPE_SFTP, 2),
    )

    port = models.PositiveIntegerField(default=21)
    host = models.TextField(null=False)
    username = models.TextField(null=False)
    password = models.TextField(null=False)
    path = models.TextField(null=False)
    connection_type = models.PositiveSmallIntegerField(default=TYPE_FTP, choices=TYPE_CHOICES, null=False)
    is_enabled = models.BooleanField(default=True)
    number_of_tries = models.PositiveSmallIntegerField(default=0)
    timeout = models.PositiveIntegerField(default=30)
    created_at = models.DateTimeField(auto_now=True, db_index=True)
