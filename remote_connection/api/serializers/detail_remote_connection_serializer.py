from rest_framework import serializers

from remote_connection.models import RemoteConnection


class DetailRemoteConnectionSerializer(serializers.ModelSerializer):
    port = serializers.IntegerField(read_only=True)
    username = serializers.CharField(read_only=True)
    host = serializers.CharField(read_only=True)
    path = serializers.CharField(read_only=True)
    connection_type = serializers.IntegerField(read_only=True)
    timeout = serializers.IntegerField(read_only=True)

    class Meta:
        model = RemoteConnection
        fields = ['port', 'username', 'host', 'path', 'connection_type', 'timeout']

