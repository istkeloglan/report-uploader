import factory
from remote_connection.models import RemoteConnection
from random import randint
from core.crypt import Crypt
import uuid


class RemoteConnectionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = RemoteConnection

    port = randint(21, 2222)
    host = 'localhost'
    username = Crypt().encrypt(str(uuid.uuid4()))
    password = Crypt().encrypt(str(uuid.uuid4()))
    path = factory.Sequence(lambda n: 'path_%s' % n)
    connection_type = factory.Iterator([RemoteConnection.TYPE_FTP, RemoteConnection.TYPE_SFTP])


class KnownRemoteConnectionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = RemoteConnection

    port = 21
    host = 'localhost'
    password = Crypt().encrypt('test')
    username = Crypt().encrypt('test')
    path = 'test'
    connection_type = RemoteConnection.TYPE_FTP
