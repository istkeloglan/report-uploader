from ftplib import FTP
from ftplib import error_perm

from .remote_connection_manager import ExceptionPathDoesNotExist
from .remote_connection_manager import ExceptionPermissionError
from .remote_connection_manager import RemoteConnectionManager


class FTPManager(RemoteConnectionManager):
    CONNECTION_FAIL_EXCEPTION = error_perm
    CONNECTION_CLOSE_FAIL_EXCEPTION = error_perm

    def get_connection(self, host, user, password, timeout, port):
        self.log(True, 'Creating the connection object')
        connection = FTP(host=host, timeout=timeout)
        FTP.port = port
        connection.set_debuglevel(0)

        self.log(True, 'The connection object is created')
        connection.login(user=user, passwd=password)

        self.log(True, 'Logged in to the server')
        self.connection = connection

    def upload_file(self):
        """ Uploads the file to given path """
        self.log(True, 'Upload file is started')
        try:
            self.connection.cwd(self.conn.path)
        except error_perm:
            self.log(False, 'The folder does not exist. ')
            raise ExceptionPathDoesNotExist()

        try:
            # if the file does already exists, adds '-1' end of the filename.
            file_check = self.connection.voidcmd('stat {}{}'.format(
                self.conn.path, self.filename_format
            ))

            if 'No such file or' not in file_check:
                self.log(True, 'The file exists. Adding a prefix')
                self.filename_format = 'New version of {}'.format(self.filename_format)

            with open(self.file_path, 'rb') as file:
                self.connection.storbinary('STOR {}'.format(self.filename_format), file)

            self.log(True, 'The file is uploaded')
        except:
            self.log(False, 'The file could not uploaded')
            raise ExceptionPermissionError()

    def quit(self):
        """ Terminates the existing connection """
        self.connection.quit()
