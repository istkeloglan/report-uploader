import paramiko

from .remote_connection_manager import RemoteConnectionManager
from .remote_connection_manager import ExceptionPathDoesNotExist
from .remote_connection_manager import ExceptionPermissionError


class SFTPManager(RemoteConnectionManager):
    CONNECTION_FAIL_EXCEPTION = paramiko.ssh_exception.AuthenticationException
    CONNECTION_CLOSE_FAIL_EXCEPTION = None

    def get_connection(self, host, user, password, timeout, port):
        self.log(True, 'Creating the connection object')
        ssh = paramiko.SSHClient()
        # pass: ssh key file
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        ssh.connect(host, port, user, password, timeout=timeout)
        self.log(True, 'The connection object is created')
        sftp = ssh.open_sftp()
        sftp.sshclient = ssh
        self.log(True, 'Logged in to the server')
        self.connection = sftp

    def upload_file(self):
        """ Uploads the file to given path """
        self.log(True, 'Upload file is started')
        try:
            self.connection.chdir(self.conn.path)
        except FileNotFoundError:
            self.log(False, 'The folder does not exist. ')
            raise ExceptionPathDoesNotExist()

        # if the file does already exists, adds '-1' end of the filename.
        try:
            self.connection.stat('{}{}'.format(
                self.conn.path, self.filename_format
            ))
            self.log(True, 'The file does exists. Adding a prefix')
            self.filename_format = 'New version of {}'.format(self.filename_format)
        except:  # pragma: no cover msg = "no need to check"
            pass
        try:
            remote_path = self.connection.normalize(self.conn.path)
            self.connection.put(self.file_path, '{}/{}'.format(remote_path, self.filename_format))
        except:
            self.log(True, 'The file could not uploaded')
            raise ExceptionPermissionError()

        self.log(True, 'The file is uploaded')

    def quit(self):
        """ Terminates the existing connection """
        self.connection.close()
