import os

import arrow
from django.conf import settings

from core.crypt import Crypt
from remote_connection.models import RemoteConnection
from task.models import TaskLog


class RemoteConnectionManager:
    def __init__(self, task_object=None, file_path=None):
        # TODO set the file permission on remote
        # TODO If the path does not exists, try 'create the folders'

        self.conn = None
        self.log_date_format = 'HH:mm:ss'
        self.file_path = file_path
        self.db_object = task_object
        self.filename_format = 'upload-{}.csv'.format(
            arrow.utcnow().to(settings.TIME_ZONE).format('YYYYMMDDHHmmssS')
        )
        self.connection = None
        self._log_messages = []
        self.status = True

    def log(self, status, message):
        if self.status:
            self.status = status
        self._log_messages.append('{time}: {message}'.format(
            time=arrow.utcnow().to(settings.TIME_ZONE).format(self.log_date_format),
            message=message,
        ))

    def run(self):
        try:
            self.connect()
            self.upload_file()
        except ExceptionPermissionError:
            self.log(False, 'Exception: the connection do not have permission')
        except ExceptionConnectionError:
            self.log(False, 'Exception: connection failed. (Retry enabled)')
            self.garbage_collection()
            raise ExceptionConnectionError()
        except ExceptionPathDoesNotExist:
            self.log(False, 'The folder does not exist, terminating')

        self.close_connection()

    def connect(self):
        """ Creates the ftp connection with given RemoteConnection Object """
        self.conn = self.db_object.connection
        assert isinstance(self.conn, RemoteConnection)
        assert os.path.exists(self.file_path)
        self.log(True, 'connect method is running')
        try:
            crypt = Crypt()
            self.get_connection(
                host=self.conn.host,
                timeout=self.conn.timeout,
                user=crypt.decrypt(self.conn.username),
                password=crypt.decrypt(self.conn.password),
                port=self.conn.port,
            )
        except self.CONNECTION_FAIL_EXCEPTION:
            self.log(False, 'Create Connection raised an exception')
            raise ExceptionConnectionError

    def close_connection(self):
        self.log(True, 'The connection is closing')
        if not self.connection:
            self.log(False, 'The connection object is none. Raising an exception')
            raise ExceptionConnectionNotFound()

        self.quit()
        self.log(True, 'The connection closed')
        self.garbage_collection()

    def garbage_collection(self, remove=True):
        TaskLog.objects.create(
            task_id=self.db_object.id,
            log="\n".join(self._log_messages),
            status=self.status
        )
        try:
            os.remove(self.file_path)
        except (OSError, FileNotFoundError):
            self.log(False, 'The uploaded file not found')
            raise ExceptionFileNotFound()


class ExceptionConnectionError(Exception):
    """ FTP Connection could not execute due to the credentials """


class ExceptionConnectionNotFound(Exception):
    """ FTP Connection does not found / None"""


class ExceptionPathDoesNotExist(Exception):
    """ Requested path does not exist in the file system """


class ExceptionFileDoesNotExist(Exception):
    """ The file does not exist in the file system """


class ExceptionPermissionError(Exception):
    """ The credential does not have a permission """


class ExceptionFileNotFound(Exception):
    """ The uploaded file does not found """
