from reports.models import Reports
import uuid
from decimal import Decimal
from random import randint

import arrow

from reports.models import Reports


def run():
    list_of_objects = []
    for i in range(0, 1000):
        list_of_objects.append(
            Reports(
                campaign_id=randint(1, 9999999),
                adgroup_id=randint(1, 9999999),
                name=str(uuid.uuid4()),
                cost=Decimal(randint(1, 245)),
                views=randint(1, 9999999),
                clicks=randint(1, 9999999),
                impressions=randint(1, 9999999),
                conversions=randint(1, 9999999),
                report_date=arrow.utcnow().shift(days=-randint(0, 300)).date(),
            )
        )
    Reports.objects.bulk_create(list_of_objects)
